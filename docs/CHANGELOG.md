## [1.1.2](https://gitlab.com/NishantTyagi/hello_india/compare/v1.1.1...v1.1.2) (2020-11-13)


### Bug Fixes

* logging changed ([59ca9a0](https://gitlab.com/NishantTyagi/hello_india/commit/59ca9a0658d508a18707ca2100b49d0f706e50cf))

## [1.1.1](https://gitlab.com/NishantTyagi/hello_india/compare/v1.1.0...v1.1.1) (2020-11-13)


### Bug Fixes

* something fixed ([07e65c9](https://gitlab.com/NishantTyagi/hello_india/commit/07e65c903b0120d484f5fb9b2b85dc8191db1cfb))

# [1.1.0](https://gitlab.com/NishantTyagi/hello_india/compare/v1.0.0...v1.1.0) (2020-11-13)


### Features

* initial config 3 ([87f1d4f](https://gitlab.com/NishantTyagi/hello_india/commit/87f1d4f080317a25212783da41c9d8d259dfdc86))

# 1.0.0 (2020-11-13)


### Features

* initial configuration ([59326cd](https://gitlab.com/NishantTyagi/hello_india/commit/59326cd30b4fa80e27e76ce134bbc402f3b1f4ea))
